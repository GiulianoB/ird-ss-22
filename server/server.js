// Import express framework (http server framework)
const { json } = require('express')
const express = require('express')
// Import pg-promise (postgres adapter package)
const pgp = require('pg-promise')()
// Import bcrypt libary to hash passwords
const bcrypt = require('bcrypt');

// Define server port
const port = 3000

// Create express instance
const app = express()

// Server static website files
app.use(express.static('public'))

// Create database adapter insance
const database = pgp({
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: 'postgres',
    database: 'ird'
})

// Configure express instance to use json data
app.use(express.json())

/**
 * Get endpoint. Request to get all sights
 */
app.get('/sights', (req, res) => {
    let queryString = 'select * from sights'
    if (req.query.format) {
        if (req.query.format === 'geojson') {
            queryString = "select json_build_object('type', 'FeatureCollection', 'features', json_agg(ST_AsGeoJSON(s.*)::json)) from sights s"
        } else if (req.query.format === 'gml') {
            queryString = "select st_asgml(geom), id, name, price from sights"
        } else {
            res.status(400).json({ message: "Bad Request!", description: "Wrong Format. Possible options: geojson or gml" })
            return
        }
    }

    database.any(queryString)
        .then(function (data) {
            if (req.query.format === 'geojson') {
                res.json(data[0].json_build_object)
            } else if (req.query.format === 'gml') {
                let xmlString = '<Features>'
                for (i = 0; i < data.length; i++) {
                    xmlString += '<Feature>'
                    xmlString += `<Property Name="id" type="Integer" value="${data[i].id}"/>`
                    xmlString += `<Property Name="name" type="String" value="${data[i].name}"/>`
                    xmlString += `<Property Name="price" type="Float" value="${data[i].price}"/>`
                    xmlString += data[i].st_asgml
                    xmlString += '</Feature>'
                }
                xmlString += '</Features>'
                res.send(xmlString)
            } else {
                res.json(data)
            }
        })
        .catch(function (error) {
            console.log(error);
            res.status(500).json({ message: 'Internal Server Error!', error: error })
        });
})

/**
 * Post endpoint. Request to create a new sight
 * Body: { name: string (required), lat: number (required), lng: number (required), price: number (optional) }
 */
app.post('/sights', (req, res) => {
    if (!req.body.name) {
        res.status(400).json({ message: "body property name is missing" })
    } else if (!req.body.lat) {
        res.status(400).json({ message: "body property lat is missing" })
    } else if (!req.body.lng) {
        res.status(400).json({ message: "body property lng is missing" })
    }
    else {
        database.one(
            "insert into sights (name, lat, lng, geom, price) values ($1, $2, $3, ST_MakePoint($2, $3), $4) returning id, name, lat, lng, geom, price",
            [req.body.name, req.body.lat, req.body.lng, req.body.price]
        )
            .then(function (data) {
                res.status(201).json(data)
            })
            .catch(function (error) {
                res.status(500).json({ message: 'Internal Server Error!', error: error })
            })
    }
})

app.patch('/sights/:id', (req, res) => {
    // TODO: geom Spalte muss noch hinzugefügt werden
    let queryString = 'update sights set '
    if(req.body.name) queryString += 'name = \'' + req.body.name + '\', '
    if(req.body.lat) queryString += 'lat = ' + req.body.lat + ', '
    if(req.body.lng) queryString += 'lng = ' + req.body.lng + ', '
    if(req.body.price) queryString += 'price = \'' + req.body.price + '\', '
    queryString = queryString.slice(0, -2)
    queryString += ' where id = ' + req.params.id + ';'

    database.none(queryString)
        .then(function () {
            res.status(200).json({ message: 'Updated!' })
        })
        .catch(function (error) {
            res.status(500).json({ message: 'Internal Server Error!', error: error })
        })
})

/**
 * Delete endpoint. Request to delete a sight by its id
 * Url Parameter: id: string (required)
 */
app.delete('/sights/:id', (req, res) => {
    database.none('delete from sights where id = $1', [req.params.id])
        .then(function () {
            res.status(204).json()
        })
        .catch(function (error) {
            res.status(500).json({ message: 'Internal Server Error!', error: error })
        })
})



/**
 * Post endpoint. Request to add a new user
 * Body: { username: string (required), password: string (required) }
 */
 app.post('/users', (req, res) => {
    bcrypt.hash(req.body.password, 10, function(err, hash) {
        if (err) {
            res.status(500).json({ message: 'Internal Server Error!', error: error })
        } else {
            database.one('insert into users (username, password) values($1, $2) returning username', [req.body.username, hash])
                .then(function (data) {
                    res.status(201).json(data)
                })
                .catch(function (error) {
                    res.status(500).json({ message: 'Internal Server Error!', error: error })
                }) 
        }
    });
})



// Start http server
app.listen(port, () => {
    console.log('http server is listening on port ' + port);
})
