<?php
 require_once("db.php");
 $arr = connectToDB::getCompaniesList();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Edit a company</title>
		<script src="js/jquery.min.js"></script>
		<link rel="stylesheet" href="css/leaflet.css" />
		<script src="js/leaflet.js"></script>
		<link rel="stylesheet" href="css/fjb_map.css" />
	</head>
	<body>
		<div id="map"></div>
		<div id = "title">Map 2021</div>
		<div id = "description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
		   <p id='loading'><span id='loadingMessage'>Loading ...</span></p>
		</div>
		<div id="sidebar"></div>
		<div id="homelink"><a href="#">Home</a></div>
		<div id="message"></div>
		<div id='providedby'>Provided by Your <a href="#">Organisation</a></div>
		<div id="status"></div>
		
		<div id="sidebar">
  
  <form action="fjb_updatecompany_db.php" method="POST">
   <h1>Edit a company</h1>
   <table cellpadding="5" cellspacing="0" border="0">
    <tbody>
     <tr align="left" valign="top">
      <td align="left" valign="top">Company name</td>
      <td align="left" valign="top">
      	<select id="company" name="company"><option value="0">Please choose a company</option>
      		<!-- ?php for($i=0; $i < count($arr); $i++) { print '<option value="'.$arr[$i]['id'].'">'.$arr[$i]['company'].'</option>'; } ? -->
      	</select>
      </td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Description</td>
      <td align="left" valign="top"><textarea id="details" name="details"></textarea></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Latitude</td>
      <td align="left" valign="top"><input id="latitude" type="text" name="latitude" /></td>
     </tr>
      <tr align="left" valign="top">
      <td align="left" valign="top">Longitude</td>
     <td align="left" valign="top"><input id="longitude" type="text" name="longitude" /></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Telephone</td>
      <td align="left" valign="top"><input id="telephone" type="text" name="telephone" /></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top"></td>
      <td align="left" valign="top"><input id='update' type="submit" value="Update"><input id='delete' type="submit" value="Delete"></td>
     </tr>
    </tbody>
   </table>
  </form>
  </div>
	
	
	<script>
	var MappingNS  = { 
		latitude : 49.0,
		longitude : 9.7,
		map : null,
		osm : null,
		url : null
	} ;

	
	MappingNS.map = L.map('map').setView([MappingNS.latitude, MappingNS.longitude], 13);

	
//	var drawControl = new L.Control.Draw({ edit: { featureGroup: featureGroup3 }, draw: { polygon: false, polyline: false, rectangle: false, circle: false, marker: { repeatMode: true } } });
	
	
	
	let osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	let osmAttrib='Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	let osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 21, attribution: osmAttrib}).addTo(MappingNS.map);  
	
	
	// Add an object to save markers
	let markers = {};


	function createMarker(lat, lng, s1, s2) {
		let newMarker = new L.marker([lat, lng], {draggable:true, zIndexOffset:900,title:s1}).addTo(MappingNS.map);
		newMarker.bindPopup("<b>" + s1 + " </b><br />" + s2);//.openPopup();
	}
	
	function createMarkerWithId(id, lat, lng, s1, s2) {
		let newMarker = new L.marker([lat, lng], {draggable:true, zIndexOffset:900,title:s1}).addTo(MappingNS.map);
		newMarker.bindPopup("<b>" + s1 + " </b><br />" + s2);//.openPopup();
		markers[id] = newMarker;
		markers[id]._icon.id = id;
		
	}
	function putDraggable() {
			/* create a draggable marker in the center of the map */
			draggableMarker = L.marker([ MappingNS.map.getCenter().lat, MappingNS.map.getCenter().lng], {draggable:true, zIndexOffset:900}).addTo(MappingNS.map);
			
			/* collect Lat,Lng values */
			draggableMarker.on('dragend', function(e) {
			 $("#lat").val(this.getLatLng().lat);
			 $("#lng").val(this.getLatLng().lng);
			});
	}

		

		
		
  $( document ).ready(function() {
	
	
	$('img.leaflet-marker-icon').on('dblclick', function(e) {
   // Use the event to find the clicked element
   var el = $(e.srcElement || e.target),
       id = el.attr('id');

    alert('Here is the markers ID: ' + id + '. Use it as you wish.')
});



	 
		$.ajax({
		url: "fjb_getcompanieslist.php",
		type: "POST",
		dataType:'json',
		data: "{'nodata' : 'are sent in this case'}"
		}).done(function(data) {
			console.log("------------  fjb_getcompanieslist -----------");
			console.log(data);
			let sel = $("select#company");
			sel.empty();
			arr = data;
			let initialMessageForSelection = "Please choose a company";
			sel.append('<option value="0">' + initialMessageForSelection + '</option>');
			for (var i=0; i<data.length; i++) {
				sel.append('<option value="' + data[i].id + '">' + data[i].company + '</option>');
				console.log("createMarker " + i + " for: " + data[i].company + " at " + data[i].latitude + "," + data[i].longitude);
				createMarkerWithId(data[i].id,data[i].latitude, data[i].longitude, data[i].company, data[i].details) ;
			}
		}
		).fail(function() {
			// Transfer fehlgeschlagen
			console.log("Fehler bei getcompanieslist");
		}).always(function() {
			console.log("AJAX getcompanieslist beendet!");
		});

		
		
		/*	
		$.post("fjb_getcompanieslist.php", { 		'nodata' : "are sent" },
		function(data) {
		console.log("------------  fjb_getcompanieslist -----------");
		console.log(data);
		var sel = $("select#company");
		sel.empty();
		for (var i=0; i<data.length; i++) {
			sel.append('<option value="' + data[i].id + '">' + "x" + data[i].company + '</option>');
			console.log(data[i].company);
		}
		}, "json");
		*/
	
    putDraggable();
    
    $("#company").change(function() {
			// show marker for the selected comapny
     for(var i=0;i<arr.length;i++) {
      if(arr[i]['id'] == $('#company').val()) {
       $('#details').val(arr[i]['details']);
       $('#latitude').val(arr[i]['latitude']);
       $('#longitude').val(arr[i]['longitude']);
       $('#telephone').val(arr[i]['telephone']);
       
       MappingNS.map.panTo([arr[i]['latitude'], arr[i]['longitude']]);
       draggableMarker.setLatLng([arr[i]['latitude'], arr[i]['longitude']]);
       draggableMarker.bindPopup ("<b>" + arr[i]['company'] + " </b><br />" + arr[i]['details']). openPopup();
       break;
      }
     }
    });
    
   });
   
//   var arr = JSON.parse( '<?php echo json_encode($arr) ?>' );
//		console.log("---->" + arr);
//		console.log(arr);

    	var fjb_action="fjb_updatecompany_db.php";		
			// fjbehr
			$('#loading').html("");
			
			$('#delete').click(function(){
    	console.log('Es wurde auf #delete geklickt');
    	fjb_action="fjb_deletecompany_db.php";
    });
    
			$("form").submit(function(event) {
				console.log("AJAX Call");
				event.preventDefault();// Das eigentliche Absenden verhindern
				// Das sendende Formular und die Metadaten bestimmen
				var form = $(this);
				var action = fjb_action; // form.attr("action"),
					method = form.attr("method"),
					data   = form.serialize();
				$.ajax({
					url: action,
					type: method,
					data: data
				}).done(function(data) {
						// Transfer erfolgreich
						$('#message').html(data);
						// alert("Empfangen: " + data);
						if (fjb_action=="fjb_deletecompanydb.php") {
							//$arr = connectToDB::getCompaniesList();
							var option = '';
							var numbers = [1, 2, 3, 4, 5];
							//for (var i=0;i<numbers.length;i++){
							//option += '<option value="'+ numbers[i] + '">' + numbers[i] + '</option>';
							//}
							$('#company').append(option);
							updateSelection();
						}
					}).fail(function() {
					// Transfer fehlgeschlagen
					alert("Fehler");
					}).always(function() {
					// Vom Transferstatus unabhängig
					alert("AJAX 1 beendet!");
					});
			});				
			function updateSelection() {
				console.log('updateSelection()');
				//event.preventDefault();// Das eigentliche Absenden verhindern
				var action = "getJsonEncodedCompaniesList.php",
					method = "post",
					data   = "";
				$.ajax({
					url: action,
					type: method,
					data: data
				}).done(function(data) {
						// Transfer erfolgreich
						$('#message').html(data);
						$('#company').empty();
						// $('#company').html('Select a company');
						if (fjb_action=="fjb_deletecompanydb.php") {
							//$arr = connectToDB::getCompaniesList();
							var option = '';
							data = eval(data);
							$('#message').html(data.length + ":" + data);
							/*
							$.each(data.values, function(i,item) {
								console.log(item.id + ':' + item.company);
			            $('#company').append( '<option value="'
			                                 + item.id
			                                 + '">'
			                                 + item.company
			                                 + '</option>' ); 
			        });
        */
							for (var i=0;i<data.length;i++){
							option += '<option value="'+ data[i].id + '">' + data[i].company + '</option>';
							}
							$('#company').append(option);
							// updateSelection();
						}
					}).fail(function() {
					// Transfer fehlgeschlagen
					alert("Fehler");
					}).always(function() {
					// Vom Transferstatus unabhängig
					alert("AJAX 2 beendet!");
					});
			}
		</script>
	</body>
</html>