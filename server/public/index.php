<?php
 require_once("db.php");
 $result = connectToDB::connectionSuccessful();
 	if($result) {
		echo("Successfully connected to DBMS.");
 	} else {
 		echo("DB error. Check configuration in file db.php and in your MySQL/MariaDB environment.\n\n"
		. "You can find the necessary SQL statements below.");
		giveAdvice();
		exit();
 	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Simple Database App</title>

    <link rel="stylesheet" href="css/style.css" />
  </head>

  <body>
    <h1>Simple CRUD Database App</h1>

    <ul>
      <li>
        <a href="fjb_addcompany_ui.php"><strong>Create</strong></a> - add an item
      </li>
      <li>
        <a href="fjb_updatecompany_ui.php"><strong>Update</strong></a> - find and update an item
      </li>
      <li>
        <a href="fjb_deletecompany_ui.php"><strong>Delete</strong></a> - delete an item
      </li>
      <li>
        <a href="getJsonEncodedCompaniesList.php"><strong>List</strong></a> - list items
      </li>
      <li>
        <a href="leafletgeocodingdigitizing.html"><strong>Map</strong></a> - Just show the map
      </li>      
    </ul>
  </body>
</html>		
<?php
function giveAdvice() {
echo("
    <hr />
    <h3>You need the MariaDB/MySQL database and some user(s)</h3>
    <pre>
    	--
--
--
CREATE DATABASE IF NOT EXISTS geodb CHARACTER SET = 'utf8';
--
--
--
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL,
  `company` varchar(60) DEFAULT NULL,
  `details` varchar(40) DEFAULT NULL,
  `longitude` decimal(19,9) DEFAULT NULL,
  `latitude` decimal(19,9) DEFAULT NULL,
  `telephone` varchar(40) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `version` int(11) NOT NULL DEFAULT 1,
  `user` varchar(40) NOT NULL DEFAULT 'student',
  `geometry` geometry NOT NULL
) DEFAULT CHARSET=utf8;

ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);



--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

--
-- 
--
CREATE USER IF NOT EXISTS 'ird'@'localhost' IDENTIFIED BY 'geheim';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON *.* TO 'ird2'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0; 
CREATE USER IF NOT EXISTS 'civ'@'localhost' IDENTIFIED BY 'geheim';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON *.* TO 'ird2'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0; 
CREATE USER IF NOT EXISTS 'pr2'@'localhost' IDENTIFIED BY 'geheim';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON *.* TO 'ird2'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0; 
		");
		}
