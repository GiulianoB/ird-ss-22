<!DOCTYPE html>
<html>
 <head>
	<head>
		<title>Add a company</title>
		<script src="js/jquery.min.js"></script>
		<link rel="stylesheet" href="css/leaflet.css" />
		<script src="js/leaflet.js"></script>
		<link rel="stylesheet" href="css/fjb_map.css" />
	</head>
 <body>
		<div id="map"></div>
		<div id = "title">Map 2021</div>
		<div id = "description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
		   <p id='loading'><span id='loadingMessage'>Loading ...</span></p>
		</div>
		<div id="sidebar"></div>
		<div id="homelink"><a href="#">Home</a></div>
		<div id="message"></div>
		<div id='providedby'>Provided by Your <a href="#">Organisation</a></div>
		<div id="status"></div>
		
		<div id="sidebar">

  <form action="fjb_addcompany_db.php" method="post">
   <h1>Add a new company</h1>
   <table cellpadding="5" cellspacing="0" border="0">
    <tbody>
     <tr align="left" valign="top">
      <td align="left" valign="top">Company name</td>
      <td align="left" valign="top"><input type="text" name="company" value='test'/></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Description</td>
      <td align="left" valign="top"><textarea name="details">A Sample company</textarea></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Latitude</td>
      <td align="left" valign="top"><input id="lat" type="text" name="latitude" /></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Longitude</td>
      <td align="left" valign="top"><input id="lng" type="text" name="longitude" /></td>
     </tr>
     <tr align="left" valign="top">
      <td align="left" valign="top">Telephone</td>
      <td align="left" valign="top"><input type="text" name="telephone" /></td>
    </tr>
    <tr align="left" valign="top">
     <td align="left" valign="top"></td>
     <td align="left" valign="top"><input type="submit" value="Save"></td>
    </tr>
   </tbody>
  </table>
 </form>
</div>
	<script>
		var draggableMarker;
		//var map = L.map('map').setView([51.505, -0.09], 13);
		
	var MappingNS  = { 
		latitude : 49.0,
		longitude : 9.7,
		map : null,
		osm : null,
		url : null
	} ;

	MappingNS.map = L.map('map').setView([MappingNS.latitude, MappingNS.longitude], 13);

	let osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	let osmAttrib='Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	let osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 21, attribution: osmAttrib}).addTo(MappingNS.map);   
  
  function onMapClick (e) {
		console.log("Es wurde an die Stelle " + e. latlng + " geklickt ");
		var lat = e.latlng.lat;
		var lng = e.latlng.lng;
    $("#lat").val(lat);
    $("#lng").val(lng);
		draggableMarker = L.marker([ lat, lng], {draggable:true, zIndexOffset:900}).addTo(MappingNS.map);
    draggableMarker.setLatLng(lat, lng);
	}
	


  function putDraggable() {
   /* create a draggable marker in the center of the map */
   draggableMarker = L.marker([ MappingNS.map.getCenter().lat, MappingNS.map.getCenter().lng], {draggable:true, zIndexOffset:900}).addTo(MappingNS.map);
   
   /* collect Lat,Lng values */
   draggableMarker.on('dragend', function(e) {
    $("#lat").val(this.getLatLng().lat);
    $("#lng").val(this.getLatLng().lng);
   });
  }
   
		$( document ).ready(function() {
			putDraggable();
			MappingNS.map.on('click ', onMapClick );

			$("form").submit(function(event) {
				console.log("AJAX Call");
				event.preventDefault();// Das eigentliche Absenden verhindern
				// Das sendende Formular und die Metadaten bestimmen
				var form = $(this);
				var action = form.attr("action"),
					method = form.attr("method"),
					data   = form.serialize();
				$.ajax({
					url: action,
					type: method,
					data: data
				}).done(function(data) {
						// Transfer erfolgreich
						$('#message').html(data);
						console.log("Empfangen: " + data);

					}).fail(function() {
					// Transfer fehlgeschlagen
					alert("Fehler");
					}).always(function() {
					// Vom Transferstatus unabhängig
					console.log("AJAX 1 beendet!");
					});
			});				


	  });
 </script>
 </body>
</html>