<?php

require_once("db.php");

$company = strip_tags($_POST['company']);
$details = strip_tags($_POST['details']);
$latitude = strip_tags($_POST['latitude']);
$longitude = strip_tags($_POST['longitude']);
$telephone = strip_tags($_POST['telephone']);

connectToDB::addCompany($company, $details, $latitude, $longitude, $telephone);
echo("<h1>Company $company added.</h1>");
?>
