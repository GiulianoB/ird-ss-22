<?php
 define("mysqlServer","localhost");
 define("mysqlDB","geodb");
 define("mysqlUser","ird");
 define("mysqlPass","geheim");

 class connectToDB {

 	public static function connectionSuccessful() {
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	try {
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
		} catch (Exception $e) {
			echo($e->getMessage());
			return false;
		} 
		return true;
	}		
	public static function addCompany( $company, $details, $latitude, $longitude, $telephone) {
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
		//if ($mysqli->connect_errno) {
		if (!$db_connection) {
    	echo "Failed to connect to MySQL: (" . $mysqli->connect_errno() . ") " . $mysqli->connect_error();
		}
		$statement = $db_connection->prepare("Insert INTO companies(company, details, latitude, longitude, telephone) VALUES(?, ?, ?, ?, ?)");
		$statement->bind_param('sssss', $company, $details, $latitude, $longitude, $telephone);
		$statement->execute();
		$statement->close();
		$db_connection->close();
	}
	
	public static function getNameById($id) {
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
//		$statement = $db_connection->prepare("Select company from companies where  where id = ?");
		$query = "Select company from companies where id = $id";
		$companyName = "unknown";
		if ($result = $db_connection->query($query)) {
			while ($row = $result->fetch_assoc()) {
				$companyName = $row['company'];
			}
		}

//		$statement->bind_param('i', $id);
//		$statement->bind_result($company);
//		$statement->execute();
//		while ($statement->fetch()) {
//			// $arr[] = [ "id" => $id, "company" => $company, "details" => $details, "latitude" => $latitude, "longitude" => $longitude, "telephone" => $telephone];
//			$companyName = $company;
//		}
//		$statement->close();
		$db_connection->close();
		return $companyName;
	}
	
	public static function getCompaniesList() {
		$arr = array();
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
		$statement = $db_connection->prepare("Select id, company, details, latitude, longitude, telephone from companies order by company ASC");
		$statement->bind_result( $id, $company, $details, $latitude, $longitude, $telephone);
		$statement->execute();
	
		while ($statement->fetch()) {
			// $arr[] = [ "id" => $id, "company" => $company, "details" => $details, "latitude" => $latitude, "longitude" => $longitude, "telephone" => $telephone];
			$arr[] = array("id" => $id, "company" => $company, "details" => $details, "latitude" => $latitude, "longitude" => $longitude, "telephone" => $telephone);
		}
		$statement->close();
		$db_connection->close();
		return $arr;
	}
	public static function getJsonEncodedCompaniesList() {
		$arr = array();
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
		$statement = $db_connection->prepare("Select id, company, details, latitude, longitude, telephone from companies order by company ASC");
		$statement->bind_result( $id, $company, $details, $latitude, $longitude, $telephone);
		$statement->execute();
		
		while ($statement->fetch()) {
			$arr[] = array("id" => $id, "company" => $company, "details" => $details, "latitude" => $latitude, "longitude" => $longitude, "telephone" => $telephone);
		}
		$statement->close();
		$db_connection->close();
		$arr = json_encode($arr);
		return $arr;
	}
	public static function updateCompany( $id, $details, $latitude, $longitude, $telephone) {
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
		$statement = $db_connection->prepare("Update companies SET details = ?,latitude = ?,longitude = ?,telephone = ? where id = ?");
		$statement->bind_param('ssssi', $details, $latitude, $longitude, $telephone, $id);
		$statement->execute();
		$statement->close();
		$db_connection->close();
	}
	public static function deleteCompany($id) {
		$db_connection = new mysqli(mysqlServer, mysqlUser, mysqlPass, mysqlDB);
		$statement = $db_connection->prepare("Delete from companies where id = ?");
		$statement->bind_param('i', $id);
		$statement->execute();
		$statement->close();
		$db_connection->close();
	}
}
?>