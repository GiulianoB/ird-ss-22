<?php
	require_once("db.php");
	$id = intval($_POST['company']);
	$name = connectToDB::getNameById($id);

	connectToDB::deleteCompany($id);
	echo("Company $name was deleted.");
?>
