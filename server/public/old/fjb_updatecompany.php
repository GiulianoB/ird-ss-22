<?php

 require_once("db.php");

 $id = intval($_POST['company']);
 $details = strip_tags($_POST['details']);
 $latitude = strip_tags($_POST['latitude']);
 $longitude = strip_tags($_POST['longitude']);
 $telephone = strip_tags($_POST['telephone']);

 connectToDB::updateCompany( $id, $details, $latitude, $longitude, $telephone);
 $name = connectToDB::getNameById($id);
 echo("Company $name was updated.");
?>
