L.Control.Line = L.Control.extend({
    onAdd: function(map) {
		
				var controlElementClass = 'fjb-leaflet-line-control'; // can be used in CSS
        var img = L.DomUtil.create('img', controlElementClass);
				
        img.src = './images/line.gif';
        img.style.width = '20px';

        return img;
    },
		onclick: function() {
			console.log('Line clicked');
			//var div = L.DomUtil.get('div_id');
			//L.DomEvent.on(div, 'click', L.DomEvent.stopPropagation);
			//MappingNS.map.off('click');
		},

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.Line = function(opts) {
    return new L.Control.Line(opts);
}

