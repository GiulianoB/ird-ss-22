-- --------------------------------------
-- Extensions
-- --------------------------------------
create extension if not exists postgis;
create extension if not exists "uuid-ossp";



-- --------------------------------------
-- Sights Tabelle
-- --------------------------------------
drop table if exists sights;
create table sights (
	id serial primary key,
	name text not null,
	lat numeric not null,
	lng numeric not null,
	geom geometry,
	price numeric
);

-- Insert einen Testdatensatz in die sights Tabelle
insert into sights (name, lat, lng, geom, price)
values ('Sehenswürdigkeit 1', 40.342, 9.234, ST_MakePoint(40.342, 9.234), 25.50);
-- Insert einen Testdatensatz in die sights Tabelle
insert into sights (name, lat, lng, geom, price)
values ('Sehenswürdigkeit 2', 40.342, 9.234,ST_MakePoint(40.342, 9.234), 25.50);
-- Insert einen Testdatensatz mit Return in die sights Tabelle
insert into sights (name, lat, lng, geom, price)
values ('Ich komme direkt aus der datenbank', 40.342, 9.234,ST_MakePoint(40.342, 9.234), 25.50)
returning id, name, lat, lng, price;

-- Select alle Datensätze der sights Tabelle
select * from sights;

-- Select as geojson
select json_build_object(
    'type', 'FeatureCollection',
    'features', json_agg(ST_AsGeoJSON(s.*)::json)
)
from sights s

-- Select gml Geometrie Elemente
select st_asgml(geom), id, name, price from sights



-- --------------------------------------
-- Users Tabelle
-- --------------------------------------
drop table if exists users;
create table users (
	id text default gen_random_uuid() primary key,
	username text unique,
	password text
);

insert into users (username, password) values('Franz', 'super-geheim');
insert into users (username, password) values('Max', 'hyper-geheim');
insert into users (username, password) values('Maria', 'hyper-geheim');
insert into users (username, password) values('Petra', '123456');




