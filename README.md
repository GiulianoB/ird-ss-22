# IRD SS 22

## Server
Öffne das Serververzeichnis mit VS Code und benütze folgende Befehle im Terminal

### Installation
```
npm install
```

### Starte den Server
```
npm run start:dev
```

### Stoppe den webserver
```
strg + c
```

## Nützliche Entwickler-Tools
- IDE: [Visual Studio Code](https://code.visualstudio.com/)
- SQL Client: [DBeaver](https://dbeaver.io/)
- Http Testing: [Postman](https://www.postman.com/downloads/)


## Quellen zum weiterlernen
- NodeJS: [Express Framework](http://expressjs.com/de/)
- PG Promise: [Examples](https://github.com/vitaly-t/pg-promise/wiki/Learn-by-Example#simple-insert)